/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Date;

/**
 *
 * @author franciscodias
 */
public class Chamado {
    private int codChamado;
    private int codTecnico;
    private String descricao;
    private String emailUsuario;
    private String telefoneSetor;
    
    public Tecnico tecnico;
    public Usuario usuario;
    
    public Chamado(){
        tecnico = new Tecnico();
    }
    
    /**
     * @return the codChamado
     */
    public int getCodChamado() {
        return codChamado;
    }

    /**
     * @param codChamado the codChamado to set
     */
    public void setCodChamado(int codChamado) {
        this.codChamado = codChamado;
    }

    /**
     * @return the codTecnico
     */
    public int getCodTecnico() {
        return codTecnico;
    }

    /**
     * @param codTecnico the codTecnico to set
     */
    public void setCodTecnico(int codTecnico) {
        this.codTecnico = codTecnico;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the emailUsuario
     */
    public String getEmailUsuario() {
        return emailUsuario;
    }

    /**
     * @param emailUsuario the emailUsuario to set
     */
    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    /**
     * @return the telefoneSetor
     */
    public String getTelefoneSetor() {
        return telefoneSetor;
    }

    /**
     * @param telefoneSetor the telefoneSetor to set
     */
    public void setTelefoneSetor(String telefoneSetor) {
        this.telefoneSetor = telefoneSetor;
    }
    
}

