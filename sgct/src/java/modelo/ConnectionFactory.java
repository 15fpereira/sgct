package modelo;

import java.sql.*;

public class ConnectionFactory {
    public Connection getConnection(){
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            return DriverManager.getConnection(
               "jdbc:mysql://localhost/bancodadossgct","root","root"
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
