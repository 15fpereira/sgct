/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Time;

/**
 *
 * @author franciscodias
 */
public class Servico {
    private int codServico;
    private String tipoServico;
    private String impactor;
    private Time tempoServico;

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public String getImpactor() {
        return impactor;
    }

    public void setImpactor(String impactor) {
        this.impactor = impactor;
    }

    public Time getTempoServico() {
        return tempoServico;
    }

    public void setTempoServico(Time tempoServico) {
        this.tempoServico = tempoServico;
    }

    public int getCodServico() {
        return codServico;
    }

    public void setCodServico(int codServico) {
        this.codServico = codServico;
    }
    
    
    
    
}
