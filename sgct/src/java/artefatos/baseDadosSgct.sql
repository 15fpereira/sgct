SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema sgct
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bancoDadosSgct` DEFAULT CHARACTER SET utf8 ;
USE `bancoDadosSgct` ;

-- -----------------------------------------------------
-- Table `bancoDadosSgct`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bancoDadosSgct`.`usuario` (
  `codUsuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `telefone` VARCHAR(20) NULL DEFAULT NULL,
  `senha` VARCHAR(20) NOT NULL,
  `dataCadastro` TIMESTAMP DEFAULT current_timestamp,
  PRIMARY KEY (`codUsuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bancoDadosSgct`.`servicos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bancoDadosSgct`.`servicos` (
  `codServico` INT(11) NOT NULL AUTO_INCREMENT,
  `tipoServico` VARCHAR(45) NOT NULL,
  `impacto` VARCHAR(45) NULL,
  `tempoServico` DATETIME not NULL,
  `dataCadastro` TIMESTAMP DEFAULT current_timestamp,
  PRIMARY KEY (`codServico`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bancoDadosSgct`.`chamdo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bancoDadosSgct`.`chamado` (
  `codChamado` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(245) NULL,
  `dataAbertura` TIMESTAMP DEFAULT current_timestamp,
  `dataFechamento` TIMESTAMP DEFAULT current_timestamp,
  `status` ENUM('a','f') NOT NULL DEFAULT 'a' COMMENT 'a para aberto;' /* comment truncated */ /*f para fechado;*/,
  `solucao` VARCHAR(245) NULL,
  `codUsuario` INT(11) NOT NULL,
  `servicos_codServico` INT(11) NOT NULL,
  PRIMARY KEY (`codChamado`),
  INDEX `fk_chamdoUsuario_idx` (`codUsuario` ASC),
  INDEX `fk_chamdo_servicos1_idx` (`servicos_codServico` ASC),
  CONSTRAINT `fk_chamdoUsuario`
    FOREIGN KEY (`codUsuario`)
    REFERENCES `bancoDadosSgct`.`usuario` (`codUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamdo_servicos1`
    FOREIGN KEY (`servicos_codServico`)
    REFERENCES `bancoDadosSgct`.`servicos` (`codServico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bancoDadosSgct`.`tecnico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bancoDadosSgct`.`tecnico` (
  `codTecnico` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `telefone` VARCHAR(15) NULL DEFAULT NULL,
  `senha` VARCHAR(20) NOT NULL,
  `dataCadastro` TIMESTAMP NULL DEFAULT current_timestamp,
  PRIMARY KEY (`codTecnico`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bancoDadosSgct`.`atendi_chamado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bancoDadosSgct`.`atendi_chamado` (
  `codChamado` INT(11) NOT NULL,
  `codTecnico` INT(11) NOT NULL,
  PRIMARY KEY (`codChamado`, `codTecnico`),
  INDEX `fk_chamdo_has_tecnico_tecnico1_idx` (`codTecnico` ASC),
  INDEX `fk_chamdo_has_tecnico_chamdo1_idx` (`codChamado` ASC),
  CONSTRAINT `fk_chamdo_has_tecnico_chamdo1`
    FOREIGN KEY (`codChamado`)
    REFERENCES `bancoDadosSgct`.`chamdo` (`codChamado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamdo_has_tecnico_tecnico1`
    FOREIGN KEY (`codTecnico`)
    REFERENCES `bancoDadosSgct`.`tecnico` (`codTecnico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
