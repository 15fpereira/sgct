/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import modelo.ConnectionFactory;
import modelo.Usuario;

/**
 *
 * @author Halley Luiz
 */
public class UsuarioDao {

    private Connection conecta;

    public UsuarioDao() {
        this.conecta = new ConnectionFactory().getConnection();
    }

    public void adicionar(Usuario usuario) {
        String sql = "insert into usuario(nome, email,senha, telefone, dataCadastro) values(?,?,?,?,?);";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);

            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getEmail());
            stmt.setString(3, usuario.getSenha());
            stmt.setString(4, usuario.getTelefone());
            stmt.setTimestamp(5, usuario.getDataCadastro());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
// esse metodo vai ser mostrado dentro da tela de solicitação de chamado
    public List<Usuario> listaUsuario() {
        String sql = "select codUsuario, nome, email,senha, telefone, dataCadastro from usuario;";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);

            List<Usuario> ListUse = new ArrayList<Usuario>();
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Usuario use = new Usuario();
                use.setCodUsuario(rs.getInt("codUsuario"));
                use.setNome(rs.getString("nome"));
                use.setEmail(rs.getString("email"));
                use.setSenha(rs.getString("senha"));
                use.setTelefone(rs.getString("telefone"));
                use.setDataCadastro(rs.getTimestamp("dataCadastro"));

                ListUse.add(use);
            }
            rs.close();
            stmt.close();

            return ListUse;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
//Esse metodo pega o usuário e add como usuario solicitante
    public Usuario pegaUsuario(int idUsuario) {
        String sql = "select codUsuario, nome, email, telefone, dataCadastro from usuario where codUsuario = " + idUsuario + ";";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            rs.next();

            Usuario use = new Usuario();
            use.setCodUsuario(rs.getInt("codUsuario"));
            use.setNome(rs.getString("nome"));
            use.setEmail(rs.getString("email"));
            use.setTelefone(rs.getString("telefone"));
            use.setDataCadastro(rs.getTimestamp("dataCadastro"));

            rs.close();
            stmt.close();
            return use;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void altUsuario(Usuario usuario) {
        String sql = "update usuario set nome=?, email=?, telefone=? where codUsuario = ?;";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);
            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getEmail());
            stmt.setString(3, usuario.getTelefone());
            stmt.setInt(4, usuario.getCodUsuario());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void excluirUsuario(Usuario usuario){
            //String sql = "drop usuario";
    }
        
}
