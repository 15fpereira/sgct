/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;
import modelo.ConnectionFactory;
import modelo.Servico;


/**
 *
 * @author franciscodias
 */
public class ServicoDao {
    private Connection conecta;

    public ServicoDao() {
        this.conecta = new ConnectionFactory().getConnection();
    }
   
    public void adicionar(Servico servico){
        String sql = "insert to servico(tipoServico, impacto, tempoServico) values(?, ?, ?);"; 
        
        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);

            stmt.setString(1, servico.getTipoServico());
            stmt.setString(2, servico.getImpactor());
            stmt.setTime(3, servico.getTempoServico());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    // Esse metodo vai ser mostrado ou listado dentro da tela de solicitação de chamado
    public List<Servico> listaServico() {
        String sql = "select tipoServico, impactor, tempoServico from servico;";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);

            List<Servico> ListServic = new ArrayList<Servico>();
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Servico servic = new Servico();
                servic.setTipoServico(rs.getString("tipoServico"));
                servic.setImpactor(rs.getString("impactor"));
                servic.setTempoServico(rs.getTime("tempoServico"));
                

                ListServic.add(servic);
            }
            rs.close();
            stmt.close();

            return ListServic;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
    
    //Esse metodo pega o o tipo de serviço e add na solicitação de chamado
    public Servico pegaServico(int idTipoServico) {
        String sql = "select tipoServico, impactor, tempoServico from servico where tipoServico = " + idTipoServico + ";";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            rs.next();

            Servico servic = new Servico();
            servic.setTipoServico(rs.getString("tipoServico"));
            servic.setImpactor(rs.getString("impactor"));
            servic.setTempoServico(rs.getTime("tempoServico"));

            rs.close();
            stmt.close();
            return servic;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void altServico(Servico servico) {
        String sql = "update servico set tipoServico=?, impactor=?, tempoServico=? where tipoServico = ?;";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);
    
            stmt.setString(1, servico.getTipoServico());
            stmt.setString(2, servico.getImpactor());
            stmt.setTime(3, servico.getTempoServico());


            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void excluirservico(Servico servico){
        
    try {  
        int excluir = JOptionPane.showConfirmDialog(null, "Tem Certeza que Deseja Excluir o Cliente?", "Confirmar Excluir", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);  
  
        if (excluir == JOptionPane.YES_OPTION) {  
  
            String sql = "Delete from servico where tipoServico =?";  
            PreparedStatement stmt = conecta.prepareStatement(sql);  
  
            stmt.setString(1, servico.getTipoServico());
            stmt.setString(2, servico.getImpactor());
            stmt.setTime(3, servico.getTempoServico()); 
            
            stmt.execute();
            stmt.close();
        }
        
    } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
}