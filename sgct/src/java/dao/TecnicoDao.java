/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import modelo.ConnectionFactory;
import modelo.Tecnico;

/**
 *
 * @author Halley Luiz
 */
public class TecnicoDao {

    private Connection conecta;

    public TecnicoDao() {
        this.conecta = new ConnectionFactory().getConnection();
    }

    public void adicionar(Tecnico tecnico) {
        String sql = "insert into tecnico(nome, email, senha, telefone, dataCadastro) values(?,?,?,?,?);";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);

            stmt.setString(1, tecnico.getNome());
            stmt.setString(2, tecnico.getEmail());
            stmt.setString(3, tecnico.getSenha());
            stmt.setString(4, tecnico.getTelefone());
            stmt.setTimestamp(5, tecnico.getDataCadastro());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
// esse metodo vai ser mostrado dentro da tela de solicitação de chamado
    public List<Tecnico> listaTecnico() {
        String sql = "select codTecnico, nome, email, senha, telefone, dataCadastro from tecnico;";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);

            List<Tecnico> ListTec = new ArrayList<Tecnico>();
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Tecnico tec = new Tecnico();
                tec.setCodTecnico(rs.getInt("codTecnico"));
                tec.setNome(rs.getString("nome"));
                tec.setEmail(rs.getString("email"));
                tec.setSenha(rs.getString("senha"));
                tec.setTelefone(rs.getString("telefone"));
                tec.setDataCadastro(rs.getTimestamp("dataCadastro"));

                ListTec.add(tec);
            }
            rs.close();
            stmt.close();

            return ListTec;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
//Esse metodo pega o usuário e add como usuario solicitante
    public Tecnico pegaTecnico(int idTecnico) {
        String sql = "select codTecnico, nome, email, telefone, dataCadastro from tecnico where codTecnico = " + idTecnico + ";";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            rs.next();

            Tecnico tec = new Tecnico();
            tec.setCodTecnico(rs.getInt("codTecnico"));
            tec.setNome(rs.getString("nome"));
            tec.setEmail(rs.getString("email"));
            tec.setTelefone(rs.getString("telefone"));
            tec.setDataCadastro(rs.getTimestamp("dataCadastro"));

            rs.close();
            stmt.close();
            return tec;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void altTecnico(Tecnico tecnico) {
        String sql = "update tecnico set nome=?, email=?, telefone=? where codTecnico = ?;";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);
            stmt.setString(1, tecnico.getNome());
            stmt.setString(2, tecnico.getEmail());
            stmt.setString(3, tecnico.getTelefone());
            stmt.setInt(4, tecnico.getCodTecnico());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void excluirTecnico(Tecnico tecnico){
            //String sql = "drop usuario";
    }
        
}
