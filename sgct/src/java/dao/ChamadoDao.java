/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Chamado;
import modelo.ConnectionFactory;
import java.util.*;

/**
 *
 * @author franciscodias
 */
public class ChamadoDao {

    private Connection conecta;

    public ChamadoDao() {
        this.conecta = new ConnectionFactory().getConnection();
    }

    public void adicionar(Chamado chamado) {
        String sql = "insert into chamado(descricao, email, telefone, codTecnico) values(?,?,?,?);";
        //String sql = "insert into "
        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);

            stmt.setString(1, chamado.getDescricao());
            stmt.setString(2, chamado.getEmailUsuario());
            stmt.setString(3, chamado.getTelefoneSetor());
            stmt.setInt(4, chamado.tecnico.getCodTecnico());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
// esse metodo vai ser mostrado dentro da tela de solicitação de chamado

    public List<Chamado> listaChamado() {
        String sql = "select \n"
                + "	c.codChamado, \n"
                + "	c.descricao,\n"
                + "	c.email, \n"
                + "	c.telefone, \n"
                + "	t.nome\n"
                + "from\n"
                + "	chamado c,\n"
                + "    tecnico t\n"
                + "where\n"
                + "	c.codTecnico = t.codTecnico and\n"
                + "status = 1;";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);

            List<Chamado> ListCham = new ArrayList<Chamado>();
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Chamado cham = new Chamado();
                cham.setCodChamado(rs.getInt("c.codChamado"));
                cham.setDescricao(rs.getString("c.descricao"));
                cham.setEmailUsuario(rs.getString("c.email"));
                cham.setTelefoneSetor(rs.getString("c.telefone"));
                cham.tecnico.setNome(rs.getString("t.nome"));

                ListCham.add(cham);
            }
            rs.close();
            stmt.close();

            return ListCham;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Chamado BuscaChamado(int id) {
        String sql = "select \n"
                + "	c.codChamado, \n"
                + "	c.descricao,\n"
                + "	c.email, \n"
                + "	c.telefone, \n"
                + "	t.nome nome\n"
                + "from\n"
                + "	chamado c,\n"
                + "    tecnico t\n"
                + "where\n"
                + "	c.codTecnico = t.codTecnico and\n"
                + "	c.codChamado = "+id+"\n"
                + ";";
        try {

            PreparedStatement stmt = conecta.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            rs.next();

            Chamado cham = new Chamado();

            cham.setCodChamado(rs.getInt("codChamado"));
            cham.setDescricao(rs.getString("descricao"));
            cham.setEmailUsuario(rs.getString("email"));
            cham.setTelefoneSetor(rs.getString("telefone"));
            cham.tecnico.setNome(rs.getString("nome"));
            
            rs.close();
            stmt.close();
            return cham;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void altChamado(Chamado chamado) {
        String sql = "update chamado set descricao=?, email=?, telefone=?, codTecnico=? where codChamado = ?;";

        try {
            PreparedStatement stmt = conecta.prepareStatement(sql);

            stmt.setString(1, chamado.getDescricao());
            stmt.setString(2, chamado.getEmailUsuario());
            stmt.setString(3, chamado.getTelefoneSetor());
            stmt.setInt(4, chamado.tecnico.getCodTecnico());
            stmt.setInt(5, chamado.getCodChamado());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void excluirChamado(Chamado chamado) {
        try {
            PreparedStatement stmt = conecta.prepareStatement("update chamado set status = 0 where codChamado = ?;");

            stmt.setLong(1, chamado.getCodChamado());
            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /*
     public Chamado pegaChamado(int idChamado) {
     String sql = "select codChamado, descricao, dataAbertura, dataFechamento, status, solucao from chamado where codChamado = " + idChamado + ";";
     try {

     PreparedStatement stmt = conecta.prepareStatement(sql);
     ResultSet rs = stmt.executeQuery();
     rs.next();

     Chamado cham = new Chamado();
     cham.setCodChamado(rs.getInt("codChamado"));
     cham.setDescricao(rs.getString("descricao"));
     cham.setDataAbertura(rs.getDate("dataAbertura"));
     cham.setDataFechamento(rs.getDate("dataFechamento"));
     cham.setStatus(rs.getString("status"));
     cham.setSolucao(rs.getString("solucao"));

     rs.close();
     stmt.close();
     return cham;

     } catch (SQLException e) {
     throw new RuntimeException(e);
     }
     }

     public void altChamado(Chamado chamado) {
     String sql = "update chamado set descricao=?, status=?, solucao=? where codChamado = ?;";

     try {
     PreparedStatement stmt = conecta.prepareStatement(sql);
     stmt.setString(1, chamado.getDescricao());
     stmt.setString(2, chamado.getStatus());
     stmt.setString(3, chamado.getSolucao());
            

     stmt.execute();
     stmt.close();
     } catch (SQLException e) {
     throw new RuntimeException(e);
     }
     }
    
     public void excluirChamado(Chamado chamado){
     //String sql = "drop usuario";
     } 
     */
}
