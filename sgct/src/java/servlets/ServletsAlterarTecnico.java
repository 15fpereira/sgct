package servlets;

import dao.TecnicoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Tecnico;

@WebServlet(name = "ServletsAlterarUsuario", urlPatterns = {"/servletsAlterarUsuario"})
public class ServletsAlterarTecnico extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Integer  id = Integer.valueOf(request.getParameter("id"));
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String telefone = request.getParameter("telefone");

        Tecnico tecnico = new Tecnico();
        tecnico.setCodTecnico(id);
        tecnico.setNome(nome);
        tecnico.setEmail(email);
        tecnico.setTelefone(telefone);

        TecnicoDao dao = new TecnicoDao();
        dao.altTecnico(tecnico);
        response.sendRedirect("visualiza-tecnico.jsp");
    }

}
