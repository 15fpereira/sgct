/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.ChamadoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Chamado;

/**
 *
 * @author OI333177
 */
@WebServlet(name = "ServletAdicionaChamado", urlPatterns = {"/servletAdicionaChamado"})
public class ServletAdicionaChamado extends HttpServlet {

  

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String descricao = request.getParameter("descricao");
        String email = request.getParameter("email");
        String telefone = request.getParameter("telefone");
        Integer id = Integer.valueOf(request.getParameter("tecnico"));
        
        out.println(descricao);
        out.println(email);
        out.println(telefone);
        out.println(id);
      
        Chamado chamado = new Chamado();
        
        
        chamado.setDescricao(descricao);
        chamado.setEmailUsuario(email);
        chamado.setTelefoneSetor(telefone);
        chamado.tecnico.setCodTecnico(id);
        
        ChamadoDao dao = new ChamadoDao();
        dao.adicionar(chamado);
        
        response.sendRedirect("confirmacao.html");
     
    }
}
