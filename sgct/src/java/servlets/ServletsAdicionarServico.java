
package servlets;

import dao.ServicoDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Servico;

/**
 *
 * @author franciscodias
 */
@WebServlet(name = "ServletsAdicionarServico", urlPatterns = {"/servletsadicionarservico"})
public class ServletsAdicionarServico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String tipoServico = request.getParameter("tipoServico");
        String impactor = request.getParameter("impactor");
        String tempServico = request.getParameter("tempoServico");
        Time tempoServico = Time.valueOf(tempServico);

        
       
        Servico servico = new Servico();
        
        servico.setTipoServico(tipoServico);
        servico.setImpactor(impactor);
        servico.setTempoServico(tempoServico);
        
        ServicoDao dao = new ServicoDao();
        
        dao.adicionar(servico);       
        response.sendRedirect("cadastro-usuario.jsp");
        
       

        
    }


}
