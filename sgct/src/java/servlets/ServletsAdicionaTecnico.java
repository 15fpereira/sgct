/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.TecnicoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Tecnico;

/**
 *
 * @author franciscodias
 */
@WebServlet(name = "ServletsAdicionaTecnico", urlPatterns = {"/servletsAdicionaTecnico"})
public class ServletsAdicionaTecnico extends HttpServlet {

    
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        String telefone = request.getParameter("telefone");
        

        
       
        Tecnico tecnico = new Tecnico();
        
        tecnico.setNome(nome);
        tecnico.setEmail(email);
        tecnico.setSenha(senha);
        tecnico.setTelefone(telefone);
        
        TecnicoDao dao = new TecnicoDao();
        
        dao.adicionar(tecnico);
        
        response.sendRedirect("cadastro-tecnico.jsp");
        
       
    }

    

}
