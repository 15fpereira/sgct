
package servlets;

import dao.ChamadoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Chamado;

/**
 *
 * @author OI333177
 */
@WebServlet(name = "ServletsExcluirChamado", urlPatterns = {"/servletsExcluirChamado"})
public class ServletsExcluirChamado extends HttpServlet {

  
   


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        int id = Integer.valueOf(request.getParameter("id"));
      
        Chamado exChamado = new Chamado();
        ChamadoDao dao = new ChamadoDao();
        exChamado.setCodChamado(id);
        dao.excluirChamado(exChamado);
        

        
        response.sendRedirect("confirmacao-exclusao.html");

     
    }


}
