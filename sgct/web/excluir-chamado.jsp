<%-- 
    Document   : excluir-chamado
    Created on : 26/11/2015, 21:46:13
    Author     : OI333177
--%>

<%@page import="modelo.Chamado"%>
<%@page import="dao.ChamadoDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listagem de Pacientes</title>


        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="css/style.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>


    </head>
    <body>
        <div class="row">
            <div class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html"><span>S.G.C.T</span></a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active">
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a href="#">Contatos</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container" id="container1">
                <div class="row">
                    <div class="container col-md-8 col-md-push-2">
                        <div class="panel panel-primary">
                            <div class="panel-heading ">
                                <h2 class="panel-title">
                                    Painel de Chamados
                                </h2>
                            </div>
                            <div class="panel-body colapso" id = "collapseExample">
                                <%
                                    int id = Integer.valueOf(request.getParameter("id"));
                                    ChamadoDao dao = new ChamadoDao();
                                    Chamado pegaChamado = dao.BuscaChamado(id);

                                    out.println("<dt>Descrição</dt>");
                                    out.println("<dd>" + pegaChamado.getDescricao() + "</dd>");
                                    out.println("<dt>E-mail</dt>");
                                    out.println("<dd>" + pegaChamado.getEmailUsuario() + "</dd>");

                                    out.println("<dt>Telefone do Setor</dt>");
                                    out.println("<dd>" + pegaChamado.getTelefoneSetor() + "</dd>");

                                    out.println("<dt>Técnico</dt>");
                                    out.println("<dd>" + pegaChamado.tecnico.getNome() + "</dd>");

                                %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <center>
        <%            
            out.println("<form action='servletsExcluirChamado' method='post'>");
            out.println("<input name='id' type='hidden' value='" + id + "'>");
            out.println("<button type='submit' class='btn btn-danger' >");
            out.println("<span class='glyphicon glyphicon-cloud-upload'></span>");
            out.println("Excluir");
            out.println("</button>");
            out.println(" <a href='listagem-paciente.jsp' class='btn btn-primary'><span class='glyphicon glyphicon-hand-up'></span> Voltar</a>");
            out.println("</form>");
            out.println("<hr/>");
        %>
    </center>


</body>
</html>
