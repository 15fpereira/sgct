

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>S.G.C.T</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="section"> 
                <div class="container"> 
                    <div class="row"> 
                        <div class="col-md-6 bg-info"> 
                            <br/>
                            <img src="img/suporte-tecnico.png" class="img-responsive center-block"> 
                            <h1>Abertura de chamado !</h1> 
                            <p>

                            </p>
                            <p>Aqui o usuário solicita abertura de chamados de suporte técnicos  
                                <br>visualização dos chamados ! Serão necessarios login e senha para 
                                <br> ter acesso. Clique no botão abaixo para fazer o login
                            </p>
                            <center>
                            <a href="login-usuario.jsp" class="btn btn-lg btn-success">Acesse aqui</a>
                            </center>
                            <br/><br/>
                        </div>
                        <div class="col-md-6 bg-success"> 
                            <br/>
                            <img src="img/suporte.png" class="img-responsive center-block"> 
                            <h1>Acesso Técnico</h1> 
                            <p></p>
                            <p>Esta área e volta somente para administradores,Tecnicos de surpote. 
                                <br>Aqui são verificados chamados técnicos, confirmação e atendimento. 
                                <br>
                                <br>
                            </p>
                            <center>
                                <a href="pagina-admin-geral.html" class="btn btn-lg btn-primary">Acesse aqui</a>
                            </center>
                            <br/><br/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>

</html>
