<%-- 
    Document   : cadastro-chamado
    Created on : 19/11/2015, 15:57:14
    Author     : franciscodias
--%>
    

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <title>Cadastro de serviços</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>SGCT</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <form action="servletsAdicionaServico" method="post">
                    <div class="row">
                        <center><h3>Cadastro de Serviço</h3></center>
                        <hr/>
                        <fieldset class="col-md-4 col-md-push-4" >
                            
                            <legend>Dados de Serviços</legend>
                            <div class="form-group">
                                <label for="tipoServico">Tipo de serviço</label>
                                <input type="text" class="form-control" id="tipoServico" name="tipoServico" placeholder="Tipo de Serviço" maxlength="55" required="Preencha este caampo!">
                            </div>
                            <div class="form-group">
                                <label for="impacto">Impacto</label>
                                <input type="impacto" class="form-control" id="impacto" name="impacto" placeholder="impacto"  required="Preencha este caampo!">
                            </div>
                            <div class="form-group">
                                <label for="tela">Tempo de estimado de solução ou SLA</label>
                                <input type="text" class="form-control" id="tempoServico" name="telefone" placeholder="00:00:00" required="Preencha este caampo!">
                            </div>
                            <hr/>
                            <center>
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-ok-sign"></span>
                                    Gravar Dados
                                </button>                      
                                <a href="pagina-suporte-tecnico.html" class=" btn btn-default">
                                    <span class="glyphicon glyphicon-arrow-left"></span> Voltar
                                </a>
                            </center>
                            <hr/>
                        </fieldset>
                    </div>

                </form>
            </div>
    </body>
</html>
