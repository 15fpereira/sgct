<%-- 
    Document   : AutenticaUsuario
    Created on : 25/11/2015, 00:21:24
    Author     : OI333177
--%>

<%@page import="modelo.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="dao.UsuarioDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String email = request.getParameter("email");
            String senha = request.getParameter("senha");

            UsuarioDao pesquisaUsuario = new UsuarioDao();
            List<Usuario> autenticaUsuario = pesquisaUsuario.listaUsuario();

            for (Usuario usuario : autenticaUsuario) {
                if (email.equals(usuario.getEmail()) && senha.equals(usuario.getSenha())) {
                    response.sendRedirect("principal-usuario.html");

                } else {
                    out.println("Usuário e senha Inválidos!");

                }

            }
        %>


    </body>
</html>
