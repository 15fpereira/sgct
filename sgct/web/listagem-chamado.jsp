<%-- 
    Document   : cadastros-paciente-boostrap
    Created on : 17/10/2015, 04:23:21
    Author     : GERSON
--%>
<%@page import="modelo.Chamado"%>
<%@page import="dao.ChamadoDao"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listagem de Pacientes</title>


         <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="css/style.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>


    </head>
    <body>
        <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>S.G.C.T</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container">


            <div class="container">
                <h3 align="center">Listagem de Chamados</h3>
                <hr/>
                <legend>Informações de Chamados</legend>
                <div style="overflow: scroll; height: 200px;">
                    <table class="table table-striped col-lg-12">
                        <tr>
                            <th class=" text-center">Código do Chamado</th>
                            <th class="">Descrição</th>
                            <th class=" text-center">e-mail do Usuario</th>
                            <th class="text-center">Telefone do Setor</th>
                            <th class="text-center">Nome do Técnico</th>
                            <th class="text-center">AÇÃO</th>
                            <th class="text-center">AÇÃO</th>

                        </tr>
                        <%
                            ChamadoDao dao = new ChamadoDao();
                            List<Chamado> listCham = dao.listaChamado();

                            for (Chamado chamado : listCham) {
                                out.println("<tr>");
                                out.println("<td class='text-center'>" + chamado.getCodChamado() + "</td>");
                                out.println("<td class=''>" + chamado.getDescricao() + "</td>");
                                out.println("<td class='text-center'>" + chamado.getEmailUsuario() + "</td>");
                                out.println("<td class='text-center'>" + chamado.getTelefoneSetor() + "</td>");
                                out.println("<td class='text-center' >" + chamado.tecnico.getNome() + "</td>");
                                out.println("<td class='text-center'>");
                                out.println("<form action='alterar-chamado.jsp' method='post'>");
                                out.println("<input name='id' type='hidden' value='" + chamado.getCodChamado() + "'>");
                                out.println("<button type='submit' class='btn btn-default' >");
                                out.println("Alterar Dados");
                                out.println("</button>");
                                out.println("</form>");
                                out.println("</td>");
                                out.println("<td class='text-center'>");
                                out.println("<form action='excluir-chamado.jsp' method='post'>");
                                out.println("<input name='id' type='hidden' value='" + chamado.getCodChamado() + "'>");
                                out.println("<button type='submit' class='btn btn-default' >");
                                out.println("Excluir");
                                out.println("</button>");
                                out.println("</form>");
                                out.println("</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </div>
                <hr/>
                <center>
                    <a href="listagem-chamado.jsp" class="btn btn-default" >
                        Atualizar Lista
                    </a>   
                    <a href="principal-usuario.html" class="btn btn-default">
                     Voltar
                    </a>

                </center>
                <hr/>
                </body>
                </html>
