<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : cadastro-chamado
    Created on : 19/11/2015, 15:57:14
    Author     : franciscodias
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.TecnicoDao" %>
<%@page import="modelo.Tecnico" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <!--<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">-->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <title>Cadastro Usuario</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>SGCT</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <form action="servletAdicionaChamado" method="post">
                    <div class="row">
                        <center><h3>Cadastro de Chamados</h3></center>
                        <hr/>
                        <fieldset class="col-md-4 col-md-push-4" >

                            <legend>Dados Pessoais</legend>
                            <div class="form-group">
                                <label for="descricao">Descreva o problemas:</label>
                                <textarea name="descricao" cols="22" rows="3" class="form-control" id="descricao" style="resize: none;"></textarea>
                        
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail"  required="Preencha este caampo!">
                            </div>
                            <div class="form-group">
                                <label for="email">Telefone do Setor</label>
                                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="(00)0000-0000" required="Preencha este caampo!">
                            </div>
                            <div class="form-group">
                                <label for="email">Selecine um tecnico</label>
                                <select class="form-control" id="tecnico" name="tecnico">
                                    <%
                                        TecnicoDao dao = new TecnicoDao();
                                        List<Tecnico> listTec = dao.listaTecnico();
                                        
                                        for (Tecnico tecnico : listTec) {
                                        out.println("<option value='" + tecnico.getCodTecnico() + "'>" + tecnico.getNome() + "</option>");
                                    }
                                    
                                    %>
                                </select>
                            </div>
                            
                            <hr/>
                            <center>
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-ok-sign"></span>
                                    Gravar Dados
                                </button>                      
                                <a href="principal-usuario.html" class=" btn btn-default">
                                    <span class="glyphicon glyphicon-arrow-left"></span> Voltar
                                </a>
                            </center>
                            <hr/>
                        </fieldset>
                    </div>

                </form>
            </div>
    </body>
</html>

