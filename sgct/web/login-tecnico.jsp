

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"><span>Help Desk</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="index.html">Página Principal</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/><br/><br/><br/>

            <div class="section"> 
                <div class="container"> 
                    <div class="row"> 
                        <div class="col-md-4 col-md-push-4">
                            <br/>
                            <legend>Página de Acesso do Técnico/suporte</legend>
                            <form role="form" action="AutenticaTecnico.jsp">
                                <div class="form-group">
                                    <label class="control-label" for="exampleInputEmail1">Login</label>
                                    <input class="form-control" name="email" id="exampleInputEmail1" placeholder="email" type="email">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="exampleInputPassword1">Senha</label>
                                    <input class="form-control" name="senha" id="exampleInputPassword1" placeholder="senha" type="password">
                                </div>
                                <button type="submit" class="btn btn-success">Logar</button>
                            </form>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>

</html>
