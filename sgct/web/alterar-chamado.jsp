<%-- 
    Document   : cadastro-chamado
    Created on : 19/11/2015, 15:57:14
    Author     : francisco dias
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.Tecnico"%>
<%@page import="dao.TecnicoDao"%>
<%@page import="modelo.Chamado"%>
<%@page import="dao.ChamadoDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <title>Cadastro Usuario</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>SGCT</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <%
                    Integer id = Integer.valueOf(request.getParameter("id"));
                    ChamadoDao dao = new ChamadoDao();
                    Chamado buscChamado = dao.BuscaChamado(id);
                   
                out.println("<form action='servletsAlterarChamado' method='post'>");
                    out.println("<div class='row'>");
                    out.println("<center><h3>Alteração de Chamado</h3></center>");
                    out.println("<hr/>");
                    out.println("<fieldset class='col-md-4 col-md-push-4'>");
                    out.println("<legend>Dados do Chamado</legend>");
                    out.println("<div class='form-group'>");
                    out.println("<label for='nome'>Descrição</label>");
                    out.println("<textarea name='descricao' cols='22' rows='3' class='form-control' id='descricao' style='resize: none;'>" + buscChamado.getDescricao() + "</textarea>");
                    out.println("</div>");
                    out.println("<div class='form-group'>");
                    out.println("<label for='Impacto'>E-mail</label>");
                    out.println("<input type='text' class='form-control' id='email' name='email' value='" + buscChamado.getEmailUsuario() + "'  maxlength='55' required='Preencha este campo!'>");
                    out.println("</div>");
                    out.println("<div class='form-group'>");
                    out.println("<label for='nome'>Telefone do Setor</label>");
                    out.println("<input type='text' class='form-control' id='telefone' name='telefone' value='" + buscChamado.getTelefoneSetor() + "'  maxlength='55' required='Preencha este campo!'>");
                    out.println("</div>");
                    out.println("<div class='form-group'>");
                    out.println("<label for='nome'>Técnico</label>: ");
                    out.println("<label for='nome'>" + buscChamado.tecnico.getNome() + "</label>");
                    out.println("<select class='form-control' id='tecnico' name='tecnico'>");

                    TecnicoDao dao1 = new TecnicoDao();
                    List<Tecnico> listTec = dao1.listaTecnico();

                    for (Tecnico tecnico : listTec) {
                        out.println("<option value='" + tecnico.getCodTecnico() + "'>" + tecnico.getNome() + "</option>");
                    }
                    out.println("</select>");
                    out.println("</div>");
                    out.println("<hr/>");
                    out.println("<center>");
                    
                    out.println(" <input type='text' hidden='id' name='id' value='" + buscChamado.getCodChamado() + "'>");
                    out.println("<button type='submit' class='btn btn-default'>");
                    out.println("<span class='glyphicon glyphicon-ok-sign'></span>");
                    out.println("Gravar Dados");
                    out.println("</button>");
                    out.println("<a href='listagem-chamado.jsp' class='btn btn-default'>");
                    out.println("<span class='glyphicon glyphicon-arrow-left'></span> Voltar");
                    out.println(" </a>");
                    out.println("</center>");
                    out.println("<hr/>");
                    out.println(" </fieldset>");
                    out.println("</div>");
                    out.println("</form>");
                    out.println("</div>");
                


                %>
            </div>
    </body>
</html>
