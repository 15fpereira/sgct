


<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="dao.TecnicoDao"  %>
<%@page import="modelo.Tecnico" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/estilo.css" type="text/css" rel="stylesheet" />


        <script type="text/javascript" src="jquery/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/fonts/glyphicons-halflings-regular.eot"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <title>sgct</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>SGCT</span></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contatos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <h3 align="center">Listagem de Técnico</h3>
                <hr/>
            </div>
            <div class="container col-md-12">

                <div style="overflow: scroll; height: 200px;">
                    <table class="table table-striped">
                        <tr>
                            <th class=" text-center" >Codigo</th>
                            <th class="" >Nome</th>
                            <th class=" text-center">E-mail</th>
                            <th class=" text-center">Telefone</th>
                              <th class=" text-center">Data de cadastro</th>
                            <th class=" text-center">Ações</th>
                        </tr>
                        <input type="text" hidden="id" name="id" value="1">
                        <%
                            TecnicoDao dao = new TecnicoDao();

                            List<Tecnico> listTec = dao.listaTecnico();
                            for (Tecnico tec : listTec) {
                                out.println("<form action='alterar-tecnico.jsp' method='post'>");
                                out.println(" <input type='text' hidden='id' name='id' value='"+tec.getCodTecnico()+"'>");
                                out.println("<tr>");
                                
                                out.println("<td class='text-center'>" + tec.getCodTecnico()+ "</td>");
                                out.println("<td>" + tec.getNome() + "</td>");
                                out.println("<td class='text-center'>" + tec.getEmail() + "</td>");
                                out.println("<td class='text-center'>" + tec.getTelefone() + "</td>");
                                out.println("<td class='text-center'>" + tec.getDataCadastro() + "</td>");
                                
                                out.println("<td class='text-center' >");
                                
                               
                                out.println("<button type='submit' class='btn btn-info'>");
                                out.println("<span class='glyphicon glyphicon-ok-sign'></span>");
                                out.println("Alterar");
                                out.println("</button> ");
                                
                                out.println("</td>");
                                
                                out.println("</tr>");
                                out.println("</form>");
                            }

                        %>
                    </table>
                </div>
                <br/>  
            </div>
            <div class="container">
                <hr/>
                <center>
                    <a href="visualiza-tecnico.jsp" class="btn btn-default" >
                        <span class="glyphicon glyphicon-refresh"></span> Atualizar Lista
                    </a>   
                    <a href="pagina-suporte-tecnico.html" class="btn btn-default">
                        <span class="glyphicon glyphicon-arrow-left"></span> Voltar
                    </a>
                </center>
                <hr/>
            </div>
    </body>
</html>
